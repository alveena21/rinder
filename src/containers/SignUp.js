import React from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';

import {
    hideMessage,
    showAuthLoader,
    userFacebookSignIn,
    userGithubSignIn,
    userGoogleSignIn,
    userSignUp,
    userTwitterSignIn
} from 'actions/Auth';

const styles = theme => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
    menu: {
        width: 200,
    },
});

const currencies = [
    {
        value: '1',
        label: 'Subject 1',
    },
    {
        value: '2',
        label: 'Subject 2',
    },
    {
        value: '3',
        label: 'Subject 3',
    },
    {
        value: '4',
        label: 'Subject 4',
    },
];

const secret_questions = [
    {
        id: '1',
        question: 'What is  your school name'
    },
    {
        id: '2',
        question: 'What is  your Pet name'
    },
    {
        id: '3',
        question: 'When is your birthday'
    }
]
const roles = [
    {
        id: '1',
        role: 'Administrator'
    },
    {
        id: '2',
        role: 'Research Provider'
    },
    {
        id: '3',
        role: 'Research Seeker'
    }
]


class SignUp extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            gender: '',
            age: '',
            location: '',
            role: '',
            institute: '',
            current_year: '',
            research_intrest: '',
            website_link: '',
            secret_question: '',
            upload_file: '',
            secrete_answer:''
        }
    }


    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    componentDidUpdate() {
        if (this.props.showMessage) {
            setTimeout(() => {
                this.props.hideMessage();
            }, 3000);
        }
        if (this.props.authUser !== null) {
            this.props.history.push('/');
        }
    }

    render() {
        const {
            name,
            email,
            password,
            confirmPassword,
            gender,
            age,
            location,
            role,
            institute,
            current_year,
            research_intrest,
            website_link,
            secret_question,
            upload_file,
            secrete_answer
        } = this.state;
        const { showMessage, loader, alertMessage, classes } = this.props;
        return (
            <div
                className="app-login-container d-flex  align-items-center animated slideInUpTiny animation-duration-3">
                <div className="app-login-main-content">
                <div className="app-logo-content d-flex align-items-center justify-content-center">
                <Link className="logo-lg" to="/" title="Jambo">
                  <img src={require("assets/images/logo.png")} alt="jambo" title="jambo"/>
                </Link>
              </div>
                    <div className="app-login-content" id="outlined-required">
                        <div className="app-login-header">
                            <h1>Sign Up</h1>
                        </div>

                        <div className="mb-8">
                            <h2><IntlMessages id="appModule.createAccount" /></h2>
                        </div>

                        <div className="app-login-form">
                            <form method="post" action="/">
                                <div className="row">
                                    <div className="col-12 col-md-6">
                                        <TextField
                                            type="text"
                                            label="Name"
                                            onChange={(event) => this.setState({ name: event.target.value })}
                                            fullWidth
                                            defaultValue={name}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                        <TextField
                                            type="email"
                                            onChange={(event) => this.setState({ email: event.target.value })}
                                            label={<IntlMessages id="appModule.email" />}
                                            fullWidth
                                            defaultValue={email}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                        <TextField
                                            type="password"
                                            onChange={(event) => this.setState({ password: event.target.value })}
                                            label={<IntlMessages id="appModule.password" />}
                                            fullWidth
                                            defaultValue={password}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                        <TextField
                                            type="password"
                                            onChange={(event) => this.setState({ confirmPassword: event.target.value })}
                                            label={<IntlMessages id="appModule.confirmPassword" />}
                                            fullWidth
                                            defaultValue={confirmPassword}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                        <FormControl component="fieldset" required>
                                            <FormLabel component="legend">Gender</FormLabel>
                                            <RadioGroup
                                                className="d-flex flex-row"
                                                aria-label="gender"
                                                name="gender"
                                                value={this.state.value}
                                                onChange={this.handleChange}
                                            >
                                                <FormControlLabel value="male1" control={<Radio color="primary" />} label="Male" />
                                                <FormControlLabel value="female1" control={<Radio color="primary" />} label="Female" />
                                                <FormControlLabel value="other1" control={<Radio color="primary" />} label="Other" />
                                                {/*<FormControlLabel value="disabled" disabled control={<Radio />} label="Disabled" />*/}
                                            </RadioGroup>
                                        </FormControl>

                                        <TextField
                                            type="text"
                                            onChange={(event) => this.setState({ age: event.target.value })}
                                            label={<IntlMessages id="appModule.age" />}
                                            fullWidth
                                            defaultValue={age}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />
                                        <TextField
                                            type="text"
                                            onChange={(event) => this.setState({ location: event.target.value })}
                                            label={<IntlMessages id="appModule.location" />}
                                            fullWidth
                                            defaultValue={location}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                    </div>
                                    <div className="col-12 col-md-6">

                                        <TextField
                                            type="text"
                                            label="Institute"
                                            onChange={(event) => this.setState({ institute: event.target.value })}
                                            fullWidth
                                            defaultValue={institute}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />
                                        <TextField
                                            id="standard-select-currency"
                                            select
                                            fullWidth
                                            label="Select Research Interest"
                                            className="mt-0 mb-2"
                                            value={this.state.currency}
                                            onChange={this.handleChange('currency')}
                                            SelectProps={{
                                                MenuProps: {
                                                    className: styles.menu,
                                                },
                                            }}
                                            // helperText="Select Research Interest  "
                                            margin="normal"
                                        >
                                            {currencies.map(option => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                        </TextField>

                                        <TextField
                                            type="text"
                                            onChange={(event) => this.setState({ current_year: event.target.value })}
                                            label={<IntlMessages id="appModule.current_year" />}
                                            defaultValue={current_year}
                                            margin="normal"
                                            className="mt-0 mb-0"
                                        />

                                        <TextField
                                            type="file"
                                            onChange={(event) => this.setState({ upload_file: event.target.value })}
                                            label={<IntlMessages id="appModule.upload_file" />}
                                            defaultValue={upload_file}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                        <TextField
                                            type="text"
                                            onChange={(event) => this.setState({ website_link: event.target.value })}
                                            label={<IntlMessages id="appModule.webite_link" />}
                                            fullWidth
                                            defaultValue={website_link}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                      
                                        <TextField
                                        id="standard-select-currency"
                                        select
                                        fullWidth
                                        label=" Secret Question" 
                                        className="mt-0 mb-2"
                                        value={this.state.secret_question}
                                        onChange={this.handleChange('secret_question')}
                                        SelectProps={{
                                            MenuProps: {
                                                className: styles.menu,
                                            },
                                        }}
                                        helperText="Select Secret Question  "
                                        margin="normal"
                                        >
                                            {secret_questions.map(option => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    {option.question}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                       
                                        <TextField
                                            type="text"
                                            onChange={(event) => this.setState({ secrete_answer: event.target.value })}
                                            label={<IntlMessages id="appModule.secret_asnwer" />}
                                            fullWidth
                                            defaultValue={secrete_answer}
                                            margin="normal"
                                            className="mt-0 mb-2"
                                        />

                                       
                                        <TextField
                                        id="standard-select-currency"
                                        select
                                        fullWidth
                                        label="Select Role  " 
                                        className="mt-0 mb-2"
                                        value={this.state.secret_question}
                                        onChange={this.handleChange('role')}
                                        SelectProps={{
                                            MenuProps: {
                                                className: styles.menu,
                                            },
                                        }}
                                        // helperText="Select Role  "
                                        margin="normal"
                                        >
                                            {roles.map(option => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    {option.role}
                                                </MenuItem>
                                            ))}
                                        </TextField>

                                    </div>

                                </div>

                                {/*
                <RadioGroup
                  aria-label="Gender"
                  name="gender1"
                  // className={styles.group}
                  value={this.state.value}
                  onChange={this.handleChange}
                >
                  <FormControlLabel value="female" control={<Radio />} label="Female" />
                  <FormControlLabel value="male" control={<Radio />} label="Male" />
                  <FormControlLabel value="other" control={<Radio />} label="Other" />
                </RadioGroup>             
*/}

                                <div className="mb-3 d-flex align-items-center justify-content-between">
                                    <Button variant="contained" onClick={() => {
                                        this.props.showAuthLoader();
                                        this.props.userSignUp({
                                            email, 
                                            password ,
                                            confirmPassword,
                                            gender,
                                            age,
                                            location,
                                            role,
                                            institute,
                                            current_year,
                                            research_intrest,
                                            website_link,
                                            secret_question,
                                            upload_file,
                                            secrete_answer});
                                    }} color="primary">
                                        <IntlMessages
                                            id="appModule.regsiter" />
                                    </Button>
                                    <Link to="/signin">
                                        <IntlMessages id="signUp.alreadyMember" />
                                    </Link>
                                </div>
                                <div className="app-social-block my-1 my-sm-3">
                                    <IntlMessages
                                        id="signIn.connectWith" />
                                    <ul className="social-link">
                                        <li>
                                            <IconButton className="icon"
                                                onClick={() => {
                                                    this.props.showAuthLoader();
                                                    this.props.userFacebookSignIn();
                                                }}>
                                                <i className="zmdi zmdi-facebook" />
                                            </IconButton>
                                        </li>

                                        <li>
                                            <IconButton className="icon"
                                                onClick={() => {
                                                    this.props.showAuthLoader();
                                                    this.props.userTwitterSignIn();
                                                }}>
                                                <i className="zmdi zmdi-twitter" />
                                            </IconButton>
                                        </li>

                                        <li>
                                            <IconButton className="icon"
                                                onClick={() => {
                                                    this.props.showAuthLoader();
                                                    this.props.userGoogleSignIn();

                                                }}>
                                                <i className="zmdi zmdi-google-plus" />
                                            </IconButton>
                                        </li>

                                        <li>
                                            <IconButton className="icon"
                                                onClick={() => {
                                                    this.props.showAuthLoader();
                                                    this.props.userGithubSignIn();
                                                }}>
                                                <i className="zmdi zmdi-github" />
                                            </IconButton>
                                        </li>
                                    </ul>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>

                {
                    loader &&
                    <div className="loader-view">
                        <CircularProgress />
                    </div>
                }
                {showMessage && NotificationManager.error(alertMessage)}
                <NotificationContainer />
            </div>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    const { loader, alertMessage, showMessage, authUser } = auth;
    return { loader, alertMessage, showMessage, authUser }
};

export default connect(mapStateToProps, {
    userSignUp,
    hideMessage,
    showAuthLoader,
    userFacebookSignIn,
    userGoogleSignIn,
    userGithubSignIn,
    userTwitterSignIn
})(SignUp);

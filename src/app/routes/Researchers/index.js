import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';

class Researchers extends React.Component {

  render() {
    return (
      <div className="app-wrapper">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.researchers"/>}/>
        <div className="d-flex justify-content-center">
          <h1><IntlMessages id="pages.researchers.description"/></h1>
        </div>

      </div>
    );
  }
}

export default Researchers;